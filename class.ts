class Report{
  companyProfile : string;

  constructor(public name : string){
    this.companyProfile = name;
  }
}

// Inheritance

class Invoice extends Report{
  constructor(public name : string , public total : number ){
    super(name);
  }

  getInvoice(){
    return `The Inoice : ${this.name} , ${this.total} `;  
  }
}



class BillOfLading extends Report{
   constructor(public name : string , public city : string, public stae : string  ){
    super(name);
  }

  printBill(){
    return `The Bill : ${this.name} , ${this.city} , ${this.stae} `;  
  }

}

var invoice = new Invoice('Google', 243);
console.log(invoice.getInvoice());

var bill = new BillOfLading('Yahoo', 'Moscow', 'moscow');
console.log(bill.printBill());