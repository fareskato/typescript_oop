var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Report = (function () {
    function Report(name) {
        this.name = name;
        this.companyProfile = name;
    }
    return Report;
}());
// Inheritance
var Invoice = (function (_super) {
    __extends(Invoice, _super);
    function Invoice(name, total) {
        var _this = _super.call(this, name) || this;
        _this.name = name;
        _this.total = total;
        return _this;
    }
    Invoice.prototype.getInvoice = function () {
        return "The Inoice : " + this.name + " , " + this.total + " ";
    };
    return Invoice;
}(Report));
var BillOfLading = (function (_super) {
    __extends(BillOfLading, _super);
    function BillOfLading(name, city, stae) {
        var _this = _super.call(this, name) || this;
        _this.name = name;
        _this.city = city;
        _this.stae = stae;
        return _this;
    }
    BillOfLading.prototype.printBill = function () {
        return "The Bill : " + this.name + " , " + this.city + " , " + this.stae + " ";
    };
    return BillOfLading;
}(Report));
var invoice = new Invoice('Google', 243);
console.log(invoice.getInvoice());
var bill = new BillOfLading('Yahoo', 'Moscow', 'moscow');
console.log(bill.printBill());
